"""Tests for plugin.py."""
import ckan.plugins
import ckan.tests.factories as factories
import paste.fixture
import ckan.model as model
import pylons.config as config
import ckan.tests.legacy as tests
import uuid
from ckanext.validation import tasks


class TestXMLValidationPlugin(object):

    API_KEY = '3c5b8868-2b70-4241-9f1d-dfddcb882718'
    # I do not know how to upload files to CKAN's resources when testing
    XML_FILE_URL = 'https://www.dropbox.com/s/i3nxpouk9hhhvmu/example-xml.xml?dl=1'

    @classmethod
    def setup_class(cls):
        model.repo.create_db()
        config['ckan.storage_path'] = '/tmp'
        # Return a test app with the custom config.
        app = ckan.config.middleware.make_app(config['global_conf'], **config)
        cls.app = paste.fixture.TestApp(app)
        cls.resource_id = str(uuid.uuid4())
        ckan.plugins.load('validation')

    @classmethod
    def teardown_class(self):
        print 'Rebuilding db...'
        model.repo.rebuild_db()

    def test_load_schema(self):
        schema_file = open('resources/xml/example-schema.xsd')
        schema_string = schema_file.read()

        dataset = factories.Dataset()
        sysadmin = factories.Sysadmin(apikey=self.API_KEY)
        resource = factories.Resource(id=self.resource_id,
                                      package_id=dataset['id'],
                                      url=self.XML_FILE_URL,
                                      format='XML', name='test-xml-resource')
        updated_resource = tests.call_action_api(
            self.app, 'resource_update', id=resource['id'],
            validation_schema=schema_string, apikey=sysadmin["apikey"],
            url=resource['url'], format=resource['format']
        )

        assert updated_resource['validation_schema'] == schema_string

    def test_validate_xml(self):
        resource_dict = tests.call_action_api(self.app, 'resource_show',
                                              id=self.resource_id)
        tasks.validate_xml(resource_dict, test_app=self.app).get()
        updated_resource_dict = tests.call_action_api(self.app,
                                                      'resource_show',
                                                      id=self.resource_id)
        print updated_resource_dict
        assert updated_resource_dict['validation'] is True
