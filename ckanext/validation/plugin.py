import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import logging
import tasks

log = logging.getLogger(__name__)


JSON_FORMAT = ['json', 'application/json']
XML_FORMAT = ['xml', 'application/xml', 'text/xml']
CSV_FORMAT = ['csv', 'text/comma-separated-values', 'text/csv',
              'application/csv']
RDF_FORMAT = ['rdf', 'application/rdf+xml', 'text/plain',
              'application/x-turtle', 'text/rdf+n3']


def validate_resource(resource):
    if resource['format'].lower() in RDF_FORMAT:
        log.debug('Validating RDF file...')
        tasks.validate_rdf.delay(resource)
    elif resource['validation_schema'] not in ['', None]:
        if resource['format'].lower() in XML_FORMAT:
            log.debug('Validating XML file...')
            tasks.validate_xml.delay(resource)
        elif resource['format'].lower() in JSON_FORMAT:
            log.debug('Validating JSON file...')
            tasks.validate_json.delay(resource)
        elif resource['format'].lower() in CSV_FORMAT:
            log.debug('Validating CSV file...')
            tasks.validate_csv.delay(resource)


class ValidationPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IResourceController)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'validation')

    def _modify_package_schema(self, schema):
        schema['resources'].update(
            {'validation_schema': [toolkit.get_validator('ignore_missing')]}
        )
        schema['resources'].update(
            {'validated': [toolkit.get_validator('ignore_missing')]}
        )
        schema['resources'].update(
            {'validation_errors': [toolkit.get_validator('ignore_missing')]}
        )
        return schema

    def create_package_schema(self):
        schema = super(ValidationPlugin, self).create_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def update_package_schema(self):
        schema = super(ValidationPlugin, self).update_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def show_package_schema(self):
        schema = super(ValidationPlugin, self).show_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return False

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []

    def after_update(self, context, resource):
        return resource

    def before_update(self, context, current, resource):
        log.debug('Validating schema...')
        validate = False
        if 'validation_schema' in resource:
            if resource['validation_schema'] == '':
                del(resource['validation_schema'])
        if ('validation_schema' not in current and
                'validation_schema' not in resource):
                validate = False
        elif ('validation_schema' not in current and
                'validation_schema' in resource):
                validate = True
        elif ('validation_schema' in current and
                'validation_schema' not in resource):
                validate = False
        elif ('validation_schema' in current and
                'validation_schema' in resource):
                    if (current['validation_schema'] !=
                            resource['validation_schema']):
                        validate = True
        log.debug('Validate %s' % validate)
        if 'format' in resource:
            if resource['format'].lower() in RDF_FORMAT:
                validate_resource(resource)
            elif validate:
                validate_resource(resource)

        return resource

    def before_show(self, resource_dict):
        if 'validation_errors' in resource_dict:
            if resource_dict['validation_errors'] != "":
                resource_dict['validation_errors_list'] = eval(
                    resource_dict['validation_errors']
                )
            else:
                resource_dict['validation_errors_list'] = None
        return resource_dict

    def before_create(self, context, resource):
        return resource

    def after_create(self, context, resource):
        if 'validation_schema' in resource:
            validate_resource(resource)
        elif 'format' in resource:
            if resource['format'] in RDF_FORMAT:
                validate_resource(resource)
        return resource

    def before_delete(self, context, resource, resources):
        return resource

    def after_delete(self, context, resources):
        return resources
