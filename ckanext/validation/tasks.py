import ConfigParser
import os
import jsonschema
import requests
from ckan.lib.celery_app import celery
from lxml import etree
import json
import logging
import redis

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

MAIN_SECTION = 'app:main'
PLUGIN_SECTION = 'plugin:validation'

SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')
API_URL = '%s/api/3' % (SITE_URL)
WELIVE_API = config.get(PLUGIN_SECTION, 'welive_api')

AAC_SECTION = 'plugin:authentication'
AAC_URL = config.get(AAC_SECTION, 'aac_url')
CLIENT_ID = config.get(AAC_SECTION, 'client_id')
CLIENT_SECRET = config.get(AAC_SECTION, 'client_secret')
REFRESH_TOKEN = config.get(AAC_SECTION, 'refresh_token')

REDIS_SECTION = 'redis'
REDIS_HOST = config.get(REDIS_SECTION, 'redis.host')
REDIS_PORT = config.get(REDIS_SECTION, 'redis.port')
REDIS_DB = config.get(REDIS_SECTION, 'redis.db')

JSON_FORMAT = ['json', 'application/json']
XML_FORMAT = ['xml', 'application/xml', 'text/xml']
CSV_FORMAT = ['csv', 'text/comma-separated-values', 'text/csv',
              'application/csv']
RDF_FORMAT = ['rdf', 'application/rdf+xml', 'text/plain',
              'application/x-turtle', 'text/rdf+n3']


def get_token():
    response = requests.post(
        '%soauth/token' % WELIVE_API,
        params={'refresh_token': REFRESH_TOKEN,
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET,
                'grant_type': 'refresh_token'}).json()
    return response


def update_resource(resource_dict):
    log.debug('Updating resource from Celery task...')
    print 'Updating resource from Celery task...'
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    token = ''
    if r.exists('welive:aac:token'):
        token = r.get('welive:aac:token')
    else:
        token_json = get_token()
        r.set('welive:aac:token', '%s %s' %
                                  (token_json['token_type'],
                                   token_json['access_token']),
              ex=token_json['expires_in'])
    url = API_URL + '/action/resource_update'
    headers = {'Authorization': token, 'Content-Type': 'application/json'}
    response = requests.post(url, data=json.dumps(resource_dict),
                             headers=headers)
    if response.status_code > 299:
        print response.content
        log.debug(response.content)


@celery.task
def validate_xml(resource_dict):
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    token = ''
    if r.exists('welive:aac:token'):
        token = r.get('welive:aac:token')
    else:
        token_json = get_token()
        r.set('welive:aac:token', '%s %s' %
                                  (token_json['token_type'],
                                   token_json['access_token']),
              ex=token_json['expires_in'])
    headers = {'Authorization': token}
    r = requests.get(resource_dict['url'], headers=headers)
    schema_root = etree.XML(resource_dict['validation_schema'])
    schema = etree.XMLSchema(schema_root)
    parser = etree.XMLParser(schema=schema)
    errors = []
    validated = False
    try:
        etree.fromstring(r.content, parser)
        validated = True
    except etree.XMLSyntaxError as e:
        errors.append(e.message)
    resource_dict['validated'] = validated
    resource_dict['validation_errors'] = errors
    update_resource(resource_dict)


@celery.task
def validate_json(resource_dict):
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    token = ''
    if r.exists('welive:aac:token'):
        token = r.get('welive:aac:token')
    else:
        token_json = get_token()
        r.set('welive:aac:token', '%s %s' %
                                  (token_json['token_type'],
                                   token_json['access_token']),
              ex=token_json['expires_in'])
    headers = {'Authorization': token}
    r = requests.get(resource_dict['url'], headers=headers)
    schema_dict = eval(resource_dict['validation_schema'])
    errors = []
    validated = False
    try:
        jsonschema.validate(r.json(), schema_dict)
        validated = True
    except jsonschema.ValidationError as e:
        errors.append(e.message)
        validated = False
    resource_dict['validated'] = validated
    resource_dict['validation_errors'] = errors
    update_resource(resource_dict)


@celery.task
def validate_csv(resource_dict):
    print 'Validating CSV...'
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    token = ''
    if r.exists('welive:aac:token'):
        token = r.get('welive:aac:token')
    else:
        token_json = get_token()
        r.set('welive:aac:token', '%s %s' %
                                  (token_json['token_type'],
                                   token_json['access_token']),
              ex=token_json['expires_in'])
    headers = {'Authorization': token}
    csv_text = requests.get(resource_dict['url'], headers=headers).content
    api_url = WELIVE_API + 'validation/csv'
    files = {'csv': csv_text, 'schema': resource_dict['validation_schema']}
    r = requests.post(api_url, files=files, verify=False)
    errors = ''
    validated = False
    if 'errors' in r.json():
        errors = r.json()['errors']
    if r.json()['result'] == 'true':
        validated = True
    else:
        validated = False
    print 'CSV validated... %s' % validated
    print errors
    resource_dict['validated'] = validated
    resource_dict['validation_errors'] = errors
    update_resource(resource_dict)


@celery.task
def validate_rdf(resource_dict):
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    token = ''
    if r.exists('welive:aac:token'):
        token = r.get('welive:aac:token')
    else:
        token_json = get_token()
        r.set('welive:aac:token', '%s %s' %
                                  (token_json['token_type'],
                                   token_json['access_token']),
              ex=token_json['expires_in'])
    headers = {'Authorization': token}
    rdf_text = requests.get(resource_dict['url'], headers=headers).content
    api_url = WELIVE_API + 'validation/rdf'
    files = {'rdf': rdf_text}
    r = requests.post(api_url, files=files, verify=False)
    errors = ''
    validated = False
    if 'errors' in r.json():
        errors = r.json()['errors']
    if r.json()['result'] == 'true':
        validated = True
    else:
        validated = False
    resource_dict['validated'] = validated
    resource_dict['validation_errors'] = errors
    update_resource(resource_dict)
