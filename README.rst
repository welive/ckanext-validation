.. You should enable this project on travis-ci.org and coveralls.io to make
   these badges work. The necessary Travis and Coverage config files have been
   generated for you.

.. image:: https://travis-ci.org/memaldi/ckanext-validation.svg?branch=master
    :target: https://travis-ci.org/memaldi/ckanext-validation

.. image:: https://coveralls.io/repos/memaldi/ckanext-validation/badge.png?branch=master
  :target: https://coveralls.io/r/memaldi/ckanext-validation?branch=master

=============
ckanext-validation
=============

An extension for CKAN for validating schema of uploaded resources.

------------
Requirements
------------

* CKAN 2.4.1

------------------------
Development Installation
------------------------

To install ckanext-validation for development, activate your CKAN virtualenv and
do::

    git clone https://github.com/memaldi/ckanext-validation.git
    cd ckanext-validation
    python setup.py develop
    pip install -r dev-requirements.txt

---------------
Config Settings
---------------

In the configuration .ini file of CKAN, include the following configuration variables::

    [plugin:validation]
    api_key = <An API key of an authorized user from CKAN>
    welive_api = <The URL of the WeLive API>
